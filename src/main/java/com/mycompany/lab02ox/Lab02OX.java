/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.lab02ox;

import java.util.Scanner;

/**
 *
 * @author asus
 */
public class Lab02OX {

    public static char[][] board = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    Scanner scan = new Scanner(System.in);
    static char currentplayer = 'x';
    static int row = 0;
    static int col = 0;

    public static void main(String[] args) {
        printWelcome();

        while (true) {
            printBoard();
            printTurn();
            inputRowcol();
            if (checkWinner()) {
                printBoard();
                printWinner();
                if (playAgain() == false) {
                    printEnded();
                    break;
                } else {
                    resetGame();
                }
            } else if (checkDraw()) {
                printBoard();
                printDraw();
                if (playAgain() == false) {
                    printEnded();
                    break;
                } else {
                    resetGame();
                }

            } else {
                switchPlayer();
            }
        }

    }

    private static void printWelcome() {
        System.out.println("- Welcome to OX games -");
        System.out.println("Start :");
    }

    private static void printBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print("|" + board[i][j] + "|");
            }
            System.out.println(" ");
        }
    }

    private static void printTurn() {
        System.out.println(currentplayer + "  Turn !!");
    }

    private static void inputRowcol() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input row,col : ");
        row = sc.nextInt();
        col = sc.nextInt();
        System.out.println(row);
        System.out.println(col);

        //checkBoardFull
        if (board[row - 1][col - 1] == '-') {
            board[row - 1][col - 1] = currentplayer;

        } else if (board[row - 1][col - 1] != '-') {
            System.out.println("This position is full, Please input another row,col: ");
            switchPlayer(); //เพื่อให้เป็นคนเดิมไม่สลับคน
        }

    }

    private static void switchPlayer() {
        if (currentplayer == 'o') {
            currentplayer = 'x';
        } else {
            currentplayer = 'o';
        }
    }

    private static boolean checkWinner() {
        if (checkRow() || checkCol() || checkX1() || checkX2()) {
            return true;
        }
        return false;
    }

    private static void printWinner() {
        System.out.println("=== Congratulations !! ===");
        printBoard();
        System.out.println("The winner is " + currentplayer + "!!!");
    }

    private static boolean checkRow() {
        for (int i = 0; i < 3; i++) {
            if (board[row - 1][i] != currentplayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkCol() {
        for (int i = 0; i < 3; i++) {
            if (board[i][col - 1] != currentplayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX1() {
        for (int i = 0; i < 3; i++) {
            if (board[i][i] != currentplayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkX2() {
        for (int i = 0; i < 3; i++) {
            if (board[i][3 - i - 1] != currentplayer) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkDraw() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }

    private static void printDraw() {
        System.out.println("This game is Draw.");
    }

    private static boolean playAgain() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Do you want to play again? (y/n)");
        String message = scan.next();
        return message.equals("y");
    }

    private static void resetGame() {
        board = new char[][] {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
        currentplayer = 'x';
    }
    
    private static void printEnded() {
        System.out.println(">> THE END <<");
    }
}
